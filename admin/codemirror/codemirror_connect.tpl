{assign var=codemirror value="load" scope="global"}
<!-- CodeMirror Files -->
<link rel="stylesheet" href="{$ABS_PATH}admin/codemirror/lib/codemirror.css">
<script src="{$ABS_PATH}admin/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="{$ABS_PATH}admin/codemirror/mode/xml/xml.js"></script>
<script src="{$ABS_PATH}admin/codemirror/mode/javascript/javascript.js"></script>
<script src="{$ABS_PATH}admin/codemirror/mode/css/css.js"></script>
<script src="{$ABS_PATH}admin/codemirror/mode/clike/clike.js"></script>
<script src="{$ABS_PATH}admin/codemirror/mode/php/php.js"></script>
<script src="{$ABS_PATH}admin/codemirror/addon/hint/show-hint.js"></script>
<link rel="stylesheet" href="{$ABS_PATH}admin/codemirror/addon/hint/show-hint.css">
<script src="{$ABS_PATH}admin/codemirror/addon/edit/closetag.js"></script>
<script src="{$ABS_PATH}admin/codemirror/addon/hint/html-hint.js"></script>
<script src="{$ABS_PATH}admin/codemirror/addon/selection/active-line.js"></script>
<script src="{$ABS_PATH}admin/codemirror/addon/dialog/dialog.js"></script>
<link rel="stylesheet" href="{$ABS_PATH}admin/codemirror/addon/dialog/dialog.css">
<script src="{$ABS_PATH}admin/codemirror/addon/search/searchcursor.js"></script>
<script src="{$ABS_PATH}admin/codemirror/addon/search/search.js"></script>
<script src="{$ABS_PATH}admin/codemirror/functions.js"></script>
<!-- / CodeMirror Files -->